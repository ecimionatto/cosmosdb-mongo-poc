package com.poc.mongo.document;

import com.mongodb.BasicDBObject;
import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.IndexOptions;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.platformlambda.core.system.AppStarter;
import org.platformlambda.core.system.Platform;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.set;

public class CosmosDBPoc {

    public static final String CONNECTION_STRING = "mongodb://conectionstring";
    public static final String DATABASE_NAME = "mercurypoc";
    public static final String COLLECTION = "instructionsetdoc";
    public static final String COSMOSDB_INSERT = "cosmosdb.insert";
    public static final String COSMOSDB_UPDATE = "cosmosdb.update";
    public static final String COSMOSDB_FIND = "cosmosdb.find";
    public static final String COSMOSDB_DELETE = "cosmosdb.delete";
    public static final String COSMOSDB_AGGREGATE = "cosmosdb.aggregate";
    final Platform platform = Platform.getInstance();


    final MongoClient mongoClient = MongoClients.create(MongoClientSettings.builder()
            .applyConnectionString(new ConnectionString(CONNECTION_STRING))
            .build());

    final MongoDatabase database = mongoClient.getDatabase(DATABASE_NAME);
    final MongoCollection<Document> collection = database.getCollection(COLLECTION);

    public static void main(String[] args) {
        AppStarter.main(args);
        new CosmosDBPoc().init();
    }

    public void init() {

        collection.createIndex(new BasicDBObject("secondaryIndex", 1), new IndexOptions().unique(true));

        try {
            platform.register(COSMOSDB_INSERT, (headers, body, instance) -> {
                collection.insertOne((Document) body);
                return null;
            }, 1);

            platform.register(COSMOSDB_UPDATE,
                    (headers, body, instance) ->
                            collection.findOneAndUpdate(eq("id", headers.get("id")),
                                    set("body", headers.get("body"))),
                    1);

            platform.register(COSMOSDB_FIND, (headers, body, instance) ->
                            collection.find(eq("id", body)).first(),
                    1);

            platform.register(COSMOSDB_AGGREGATE, (headers, body, instance) ->
                    collection.aggregate((List<? extends Bson>) body)
                            .into(new ArrayList<>()), 1);

            platform.register(COSMOSDB_DELETE, (headers, body, instance) ->
                            collection.findOneAndDelete(eq("id", body)),
                    1);

        } catch (IOException e) {
            throw new IllegalStateException(e);
        }


    }

}
