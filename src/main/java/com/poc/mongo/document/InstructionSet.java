package com.poc.mongo.document;

import org.bson.Document;

import java.util.Optional;

public class InstructionSet extends Document {

    public static final String ID = "id";
    public static final String BODY = "body";
    public static final String USERNAME = "username";

    public InstructionSet(String id, String body) {
        this.append(ID, id);
        this.append(BODY, body);
    }

    public InstructionSet(final String id, final String body, final String username) {
        this.append(ID, id);
        this.append(BODY, body);
        this.append(USERNAME, username);
    }

    public String getBody() {
        return this.get(BODY, String.class);
    }

    public String getId() {
        return this.get(ID, String.class);
    }

    public Optional<String> getUsername() {
        return Optional.ofNullable(this.get(USERNAME, String.class));
    }

}
