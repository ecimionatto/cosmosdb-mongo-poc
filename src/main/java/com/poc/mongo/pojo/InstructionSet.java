package com.poc.mongo.pojo;

import com.google.common.base.Objects;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.bson.codecs.pojo.annotations.BsonCreator;
import org.bson.codecs.pojo.annotations.BsonId;
import org.bson.codecs.pojo.annotations.BsonProperty;

import java.math.BigDecimal;
import java.util.List;

public class InstructionSet {

    public static final String USERNAME = "username";
    public static final String INSTRUCTION_SET_ID = "_id";
    public static final String QUOTES = "quotes";
    public static final String QUOTE_ID = "quote_id";
    public static final String QUOTE_PRICE = "price";


    @BsonId
    @BsonProperty(value = INSTRUCTION_SET_ID)
    private final String instructionSetId;

    @BsonProperty(value = QUOTES)
    private final List<Quote> quotes;

    @BsonProperty(value = USERNAME)
    private final String username;

    @BsonCreator
    public InstructionSet(
            @BsonProperty(value = INSTRUCTION_SET_ID) final String instructionSetId,
            @BsonProperty(value = QUOTES) final List<Quote> quotes,
            @BsonProperty(value = USERNAME) final String username) {
        this.instructionSetId = instructionSetId;
        this.quotes = quotes;
        this.username = username;
    }

    public InstructionSet(final String instructionSetId, final List<Quote> quotes) {
        this.instructionSetId = instructionSetId;
        this.quotes = quotes;
        this.username = null;
    }

    public String getInstructionSetId() {
        return instructionSetId;
    }

    public List<Quote> getQuotes() {
        return quotes;
    }

    public String getUsername() {
        return username;
    }

    public InstructionSet withQuotes(final List<Quote> quotes) {
        return new InstructionSet(this.instructionSetId, quotes, this.username);
    }

    public static class Quote {
        @BsonProperty(value = QUOTE_ID)
        private final String id;
        @BsonProperty(value = QUOTE_PRICE)
        private final BigDecimal price;

        @BsonCreator
        public Quote(@BsonProperty(value = QUOTE_ID) final String id,
                     @BsonProperty(value = QUOTE_PRICE) final BigDecimal price) {
            this.id = id;
            this.price = price;
        }

        public String getId() {
            return id;
        }

        public BigDecimal getPrice() {
            return price;
        }

        @Override
        public boolean equals(final Object o) {
            return EqualsBuilder.reflectionEquals(this, o);
        }

    }

    @Override
    public boolean equals(final Object o) {
        return EqualsBuilder.reflectionEquals(this, o);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(instructionSetId, quotes, username);
    }
}
