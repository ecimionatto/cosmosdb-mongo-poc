package com.poc.mongo.pojo;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.ClassModel;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.bson.conversions.Bson;
import org.platformlambda.core.models.TypedLambdaFunction;
import org.platformlambda.core.system.AppStarter;
import org.platformlambda.core.system.Platform;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.addToSet;
import static com.mongodb.client.model.Updates.set;
import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

public class CosmosPojoDBPoc {

    public static final String CONNECTION_STRING = "";
    public static final String DATABASE_NAME = "mercurypoc";
    public static final String COLLECTION = "instructionsetpojo";
    public static final String COSMOSDB_INSERT = "cosmosdb.insert";
    public static final String COSMOSDB_REPLACE = "cosmosdb.update";
    public static final String COSMOSDB_UPDATE_PARTIAL_QUOTE = "cosmosdb.update.partial.quote";
    public static final String COSMOSDB_ADD_QUOTE = "cosmosdb.add.quote";
    public static final String COSMOSDB_FIND = "cosmosdb.find";
    public static final String COSMOSDB_DELETE = "cosmosdb.delete";
    public static final String COSMOSDB_AGGREGATE = "cosmosdb.aggregate";
    public static final int INSTANCES = 100;
    private final Platform platform = Platform.getInstance();


    final private ClassModel<InstructionSet> classModel = ClassModel.builder(InstructionSet.class)
            .enableDiscriminator(true).build();

    private final CodecRegistry pojoCodecRegistry = fromProviders(PojoCodecProvider.builder()
            .register(classModel).automatic(true).build());

    private final CodecRegistry codecRegistry = fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
            pojoCodecRegistry);


    final private MongoClient mongoClient = MongoClients.create(MongoClientSettings.builder()
            .applyConnectionString(new ConnectionString(CONNECTION_STRING))
            .codecRegistry(codecRegistry)
            .build());

    final private MongoDatabase database = mongoClient.getDatabase(DATABASE_NAME);
    final private MongoCollection<InstructionSet> collection = database.getCollection(COLLECTION, InstructionSet.class);


    public static void main(String[] args) {
        AppStarter.main(args);
        new CosmosPojoDBPoc().init();
    }

    public void init() {

        //collection.createIndex(new BasicDBObject(InstructionSet.INSTRUCTION_SET_ID, 1), new IndexOptions().unique(true));

        try {
            platform.register(COSMOSDB_INSERT, (TypedLambdaFunction<InstructionSet, Void>)
                    (headers, body, instance) -> {
                        collection.insertOne(body);
                        return null;
                    }, INSTANCES);

            platform.register(COSMOSDB_REPLACE, (TypedLambdaFunction<InstructionSet, InstructionSet>)
                            (headers, body, instance) ->
                                    collection.findOneAndReplace(eq(InstructionSet.INSTRUCTION_SET_ID, body.getInstructionSetId()),
                                            body),
                    INSTANCES);

            platform.register(COSMOSDB_UPDATE_PARTIAL_QUOTE, (TypedLambdaFunction<InstructionSet, Void>)
                    (headers, body, instance) -> {
                        body.getQuotes().stream().forEach(quote ->
                                collection.findOneAndUpdate(and(eq(InstructionSet.INSTRUCTION_SET_ID, body.getInstructionSetId()),
                                        eq(InstructionSet.QUOTES + "." + InstructionSet.QUOTE_ID, quote.getId())),
                                        set(InstructionSet.QUOTES + ".$." + InstructionSet.QUOTE_PRICE, quote.getPrice())
                                ));
                        return null;
                    }, INSTANCES);

            platform.register(COSMOSDB_ADD_QUOTE, (TypedLambdaFunction<InstructionSet, Void>)
                    (headers, body, instance) -> {
                        body.getQuotes().stream().forEach(quote ->
                                collection.findOneAndUpdate(eq(InstructionSet.INSTRUCTION_SET_ID, body.getInstructionSetId()),
                                        addToSet(InstructionSet.QUOTES, quote)
                                ));
                        return null;
                    }, INSTANCES);

            platform.register(COSMOSDB_FIND, (TypedLambdaFunction<String, InstructionSet>)
                            (headers, id, instance) ->
                                    collection.find(eq(InstructionSet.INSTRUCTION_SET_ID, id)).first(),
                    INSTANCES);

            platform.register(COSMOSDB_AGGREGATE, (TypedLambdaFunction<List<? extends Bson>, List<InstructionSet>>)
                    (headers, body, instance) ->
                            collection.aggregate(body)
                                    .into(new ArrayList<>()), 1);

            platform.register(COSMOSDB_DELETE, (TypedLambdaFunction<String, InstructionSet>)
                            (headers, id, instance) ->
                                    collection.findOneAndDelete(eq(InstructionSet.INSTRUCTION_SET_ID, id)),
                    INSTANCES);

        } catch (IOException e) {
            throw new IllegalStateException(e);
        }


    }

}
