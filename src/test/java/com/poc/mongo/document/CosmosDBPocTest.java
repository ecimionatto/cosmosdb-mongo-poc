package com.poc.mongo.document;

import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Projections;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.platformlambda.core.exception.AppException;
import org.platformlambda.core.models.EventEnvelope;
import org.platformlambda.core.models.Kv;
import org.platformlambda.core.system.PostOffice;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeoutException;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

import static com.mongodb.client.model.Aggregates.match;
import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.or;
import static com.poc.mongo.document.CosmosDBPoc.COSMOSDB_FIND;
import static com.poc.mongo.document.CosmosDBPoc.COSMOSDB_INSERT;
import static java.util.stream.Collectors.toList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class CosmosDBPocTest {

    @BeforeAll
    public static void setUp() {
        CosmosDBPoc.main(null);
    }

    @Test
    void shouldFindAfterInserting() throws IOException, TimeoutException, AppException {
        final InstructionSet instructionSet = new InstructionSet(UUID.randomUUID().toString(),
                "test");

        PostOffice.getInstance().request(
                COSMOSDB_INSERT,
                10000,
                instructionSet);

        EventEnvelope result = PostOffice.getInstance().request(
                COSMOSDB_FIND,
                10000,
                instructionSet.getId());

        assertEquals("test", ((Document)
                result.getBody()).get("body"));
    }


    @Test
    void shouldFailWhenInsertingDuplicate() throws IOException, TimeoutException, AppException {
        final InstructionSet instructionSet = new InstructionSet(UUID.randomUUID().toString(),
                "test");

        PostOffice.getInstance().request(
                COSMOSDB_INSERT,
                10000,
                instructionSet);

        Assertions.assertThrows(AppException.class, () ->
                PostOffice.getInstance().request(
                        COSMOSDB_INSERT,
                        10000,
                        instructionSet));

    }


    @Test
    void shouldFindByIds() throws IOException, TimeoutException, AppException, InterruptedException {

        final List<String> ids = IntStream.range(0, 5)
                .mapToObj(i -> UUID.randomUUID().toString())
                .collect(toList());

        ids.forEach(id -> {
            final InstructionSet instructionSet = new InstructionSet(id,
                    "body");
            try {
                PostOffice.getInstance().request(
                        COSMOSDB_INSERT,
                        10000,
                        instructionSet);
            } catch (IOException | TimeoutException | AppException e) {
                throw new AssertionError(e);
            }
        });

        final List<Bson> filters = ids.stream()
                .map(id -> eq("id", id))
                .collect(toList());

        EventEnvelope response = PostOffice.getInstance().request(
                CosmosDBPoc.COSMOSDB_AGGREGATE,
                10000,
                Arrays.asList(match(or(filters))));

        assertEquals(5, ((List<Document>) response.getBody()).size());

    }

    @Test
    void shouldExcludeIdAndFindUsernamesByIdAndUsernameUsingRegex() throws IOException, TimeoutException, AppException, InterruptedException {

        final String id = UUID.randomUUID().toString();

        final InstructionSet instructionSet = new InstructionSet(id,
                "body", "username" + id);

        PostOffice.getInstance().request(
                COSMOSDB_INSERT,
                10000,
                instructionSet);

        Pattern regex = Pattern.compile(id, Pattern.CASE_INSENSITIVE);

        final EventEnvelope response = PostOffice.getInstance().request(
                CosmosDBPoc.COSMOSDB_AGGREGATE,
                10000,
                Arrays.asList(
                        Aggregates.project(Projections.fields(Projections.excludeId())),
                        match(and(eq("id", id),
                                Filters.regex("username", regex)))));

        final List<Document> documents = (List<Document>) response.getBody();
        assertEquals(1, documents.size());
        assertEquals("username" + id,
                documents.get(0).get("username"));
        assertNull(
                documents.get(0).get("_id"));
    }

    @Test
    void shouldFindAfterUpdate() throws IOException, TimeoutException, AppException {
        final InstructionSet instructionSet = new InstructionSet(UUID.randomUUID().toString(),
                "test");

        PostOffice.getInstance().request(
                COSMOSDB_INSERT,
                10000,
                instructionSet);

        EventEnvelope result = PostOffice.getInstance().request(
                COSMOSDB_FIND,
                10000,
                instructionSet.getId());
        Document document = (Document) result.getBody();
        assertEquals("test", document.get("body"));

        PostOffice.getInstance().request(
                CosmosDBPoc.COSMOSDB_UPDATE,
                10000,
                new Kv("id", instructionSet.getId()),
                new Kv("body", "updated"));

        final EventEnvelope resultUpdated = PostOffice.getInstance().request(
                COSMOSDB_FIND,
                10000,
                instructionSet.getId());

        final Document updatedBody = (Document) resultUpdated.getBody();
        assertEquals("updated", updatedBody.get("body"));

    }

    @Test
    void shouldDeleteAfterInserting() throws IOException, TimeoutException, AppException {
        final InstructionSet instructionSet = new InstructionSet(UUID.randomUUID().toString(),
                "test");

        PostOffice.getInstance().request(
                COSMOSDB_INSERT,
                10000,
                instructionSet);

        EventEnvelope result = PostOffice.getInstance().request(
                COSMOSDB_FIND,
                10000,
                instructionSet.getId());
        assertEquals("test", ((Document) result.getBody())
                .get("body"));

        PostOffice.getInstance().request(
                CosmosDBPoc.COSMOSDB_DELETE,
                10000,
                instructionSet.getId());

        EventEnvelope resultAfterDelete = PostOffice.getInstance().request(
                COSMOSDB_FIND,
                10000,
                instructionSet.getId());
        assertNull(resultAfterDelete.getBody());

    }

    @Test
    void shouldIgnoreConflict() throws IOException, TimeoutException, AppException {
        final InstructionSet first = new InstructionSet(UUID.randomUUID().toString(),
                "test");

        PostOffice.getInstance().request(
                COSMOSDB_INSERT,
                10000,
                first);

        EventEnvelope dirty = PostOffice.getInstance().request(
                COSMOSDB_FIND,
                10000,
                first.getId());

        final InstructionSet second = new InstructionSet(first.getId(),
                "updated body");

        PostOffice.getInstance().request(
                CosmosDBPoc.COSMOSDB_UPDATE,
                10000,
                new Kv("-id", second.getId()),
                new Kv("body", "updated 1"));

        PostOffice.getInstance().request(
                CosmosDBPoc.COSMOSDB_UPDATE,
                10000,
                new Kv("id", dirty.getId()),
                new Kv("body", "updated 2"));
    }


}