package com.poc.mongo.pojo;

import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Projections;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.platformlambda.core.exception.AppException;
import org.platformlambda.core.models.EventEnvelope;
import org.platformlambda.core.system.PostOffice;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeoutException;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

import static com.mongodb.client.model.Aggregates.match;
import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.or;
import static com.poc.mongo.pojo.CosmosPojoDBPoc.COSMOSDB_FIND;
import static com.poc.mongo.pojo.CosmosPojoDBPoc.COSMOSDB_INSERT;
import static java.util.stream.Collectors.toList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

class CosmosPojoDBPocTest {

    @BeforeAll
    public static void setUp() {
        CosmosPojoDBPoc.main(null);
    }

    @Test
    void shouldFindAfterInserting() throws IOException, TimeoutException, AppException {
        final InstructionSet first = new InstructionSet(UUID.randomUUID().toString(),
                List.of(new InstructionSet.Quote(UUID.randomUUID().toString(), BigDecimal.valueOf(12.23))));

        PostOffice.getInstance().request(
                COSMOSDB_INSERT,
                10000,
                first);

        EventEnvelope result = PostOffice.getInstance().request(
                COSMOSDB_FIND,
                10000,
                first.getInstructionSetId());

        assertEquals(first,
                result.getBody());
    }


    @Test
    void shouldFailWhenInsertingDuplicate() throws IOException, TimeoutException, AppException {
        final InstructionSet instructionSet = new InstructionSet(UUID.randomUUID().toString(),
                List.of(new InstructionSet.Quote(UUID.randomUUID().toString(), BigDecimal.valueOf(12.23))));

        PostOffice.getInstance().request(
                COSMOSDB_INSERT,
                10000,
                instructionSet);

        EventEnvelope result = PostOffice.getInstance().request(
                COSMOSDB_FIND,
                10000,
                instructionSet.getInstructionSetId());

        Assertions.assertThrows(AppException.class, () ->
                PostOffice.getInstance().request(
                        COSMOSDB_INSERT,
                        10000,
                        result.getBody()));

    }

    @Test
    void shouldFindByIds() throws IOException, TimeoutException, AppException {

        final List<String> ids = IntStream.range(0, 5)
                .mapToObj(i -> UUID.randomUUID().toString())
                .collect(toList());

        ids.forEach(id -> {
            final InstructionSet instructionSet = new InstructionSet(id,
                    List.of(new InstructionSet.Quote(UUID.randomUUID().toString(), BigDecimal.valueOf(12.23))));
            try {
                PostOffice.getInstance().request(
                        COSMOSDB_INSERT,
                        10000,
                        instructionSet);
                System.out.println(id);
            } catch (IOException | TimeoutException | AppException e) {
                throw new AssertionError(e);
            }
        });

        final List<Bson> filters = ids.stream()
                .map(id -> eq(InstructionSet.INSTRUCTION_SET_ID, id))
                .collect(toList());

        EventEnvelope response = PostOffice.getInstance().request(
                CosmosPojoDBPoc.COSMOSDB_AGGREGATE,
                10000,
                Arrays.asList(match(or(filters))));

        assertEquals(5, ((List<Document>) response.getBody()).size());

    }

    @Test
    void shouldExcludeIdAndFindUsernamesByIdAndUsernameUsingRegex() throws IOException, TimeoutException, AppException {

        final String id = UUID.randomUUID().toString();

        final InstructionSet instructionSet = new InstructionSet(id,
                List.of(new InstructionSet.Quote(UUID.randomUUID().toString(), BigDecimal.valueOf(12.23))),
                "username" + id);

        PostOffice.getInstance().request(
                COSMOSDB_INSERT,
                10000,
                instructionSet);

        Pattern regex = Pattern.compile(id, Pattern.CASE_INSENSITIVE);

        final EventEnvelope response = PostOffice.getInstance().request(
                CosmosPojoDBPoc.COSMOSDB_AGGREGATE,
                10000,
                Arrays.asList(
                        Aggregates.project(Projections.fields(Projections.include(InstructionSet.USERNAME))),
                        match(and(eq(InstructionSet.INSTRUCTION_SET_ID, id),
                                Filters.regex(InstructionSet.USERNAME, regex)))));

        final List<InstructionSet> documents = (List<InstructionSet>) response.getBody();
        assertEquals(1, documents.size());
        assertEquals("username" + id,
                instructionSet.getUsername());
        assertEquals(id,
                instructionSet.getInstructionSetId());
    }

    @Test
    void shouldFindAfterUpdate() throws IOException, TimeoutException, AppException {
        final InstructionSet instructionSet = new InstructionSet(UUID.randomUUID().toString(),
                List.of(new InstructionSet.Quote(UUID.randomUUID().toString(), BigDecimal.valueOf(12.23))));

        PostOffice.getInstance().request(
                COSMOSDB_INSERT,
                10000,
                instructionSet);

        EventEnvelope result = PostOffice.getInstance().request(
                COSMOSDB_FIND,
                10000,
                instructionSet.getInstructionSetId());
        InstructionSet document = (InstructionSet) result.getBody();

        assertEquals(BigDecimal.valueOf(12.23), document.getQuotes().get(0).getPrice());

        final InstructionSet instructionSetReplacement = new InstructionSet(instructionSet.getInstructionSetId(),
                List.of(new InstructionSet.Quote(UUID.randomUUID().toString(), BigDecimal.valueOf(15.87))));

        PostOffice.getInstance().request(
                CosmosPojoDBPoc.COSMOSDB_REPLACE,
                10000,
                instructionSetReplacement);

        final EventEnvelope resultUpdated = PostOffice.getInstance().request(
                COSMOSDB_FIND,
                10000,
                instructionSet.getInstructionSetId());

        final InstructionSet updatedBody = (InstructionSet) resultUpdated.getBody();
        assertEquals(BigDecimal.valueOf(15.87), updatedBody.getQuotes().get(0).getPrice());

    }

    @Test
    void shouldDeleteAfterInserting() throws IOException, TimeoutException, AppException {
        final InstructionSet instructionSet = new InstructionSet(UUID.randomUUID().toString(),
                List.of(new InstructionSet.Quote(UUID.randomUUID().toString(), BigDecimal.valueOf(12.23))));

        PostOffice.getInstance().request(
                COSMOSDB_INSERT,
                10000,
                instructionSet);

        EventEnvelope result = PostOffice.getInstance().request(
                COSMOSDB_FIND,
                10000,
                instructionSet.getInstructionSetId());
        assertEquals(BigDecimal.valueOf(12.23), ((InstructionSet) result
                .getBody()).getQuotes().get(0).getPrice());

        PostOffice.getInstance().request(
                CosmosPojoDBPoc.COSMOSDB_DELETE,
                10000,
                instructionSet.getInstructionSetId());

        EventEnvelope resultAfterDelete = PostOffice.getInstance().request(
                COSMOSDB_FIND,
                10000,
                instructionSet.getInstructionSetId());
        assertNull(resultAfterDelete.getBody());

    }

    @Test
    public void shouldInsertInstruction_AndSetWithQuotes() throws TimeoutException, IOException, AppException {
        final InstructionSet first = new InstructionSet(UUID.randomUUID().toString(),
                List.of(new InstructionSet.Quote(UUID.randomUUID().toString(), BigDecimal.valueOf(12.23))));

        PostOffice.getInstance().request(
                COSMOSDB_INSERT,
                10000,
                first);

        EventEnvelope result = PostOffice.getInstance().request(
                COSMOSDB_FIND,
                10000,
                first.getInstructionSetId());

        InstructionSet instructionSet = (InstructionSet) result
                .getBody();
        assertEquals(1, instructionSet.getQuotes().size());
        assertEquals(BigDecimal.valueOf(12.23), instructionSet.getQuotes().get(0).getPrice());

        InstructionSet updatedInstructionSet = instructionSet.withQuotes(
                List.of(new InstructionSet.Quote(first.getQuotes().get(0).getId(), BigDecimal.valueOf(15.01))));

        PostOffice.getInstance().request(
                CosmosPojoDBPoc.COSMOSDB_UPDATE_PARTIAL_QUOTE,
                10000,
                updatedInstructionSet);

        EventEnvelope updatedResult = PostOffice.getInstance().request(
                COSMOSDB_FIND,
                10000,
                first.getInstructionSetId());
        assertEquals(1, ((InstructionSet) updatedResult
                .getBody()).getQuotes().size());
        assertEquals(BigDecimal.valueOf(15.01), ((InstructionSet) updatedResult
                .getBody()).getQuotes().get(0).getPrice());

    }

    @Test
    public void concurrentUpdates() throws TimeoutException, IOException, AppException {

        final InstructionSet first = new InstructionSet(UUID.randomUUID().toString(),
                List.of(new InstructionSet.Quote(UUID.randomUUID().toString(), BigDecimal.valueOf(12.23))));

        PostOffice.getInstance().request(
                COSMOSDB_INSERT,
                10000,
                first);

        IntStream.range(0, 1000)
                .parallel()
                .forEach(price -> {

                    final InstructionSet updatedInstructionSet = first.withQuotes(
                            List.of(new InstructionSet.Quote(first.getQuotes().get(0).getId(), BigDecimal.valueOf(15.01))));

                    try {
                        PostOffice.getInstance().request(
                                CosmosPojoDBPoc.COSMOSDB_UPDATE_PARTIAL_QUOTE,
                                60000,
                                updatedInstructionSet);
                    } catch (IOException | TimeoutException | AppException e) {
                        throw new RuntimeException(e);
                    }

                });

        EventEnvelope updatedResult = PostOffice.getInstance().request(
                COSMOSDB_FIND,
                30000,
                first.getInstructionSetId());
        final List<InstructionSet.Quote> quotes = ((InstructionSet) updatedResult
                .getBody()).getQuotes();
        assertEquals(1, quotes.size());

        final InstructionSet.Quote quote = quotes.get(0);
        assertEquals(first.getQuotes().get(0).getId(), quote.getId());
        assertNotNull(first.getQuotes().get(0).getPrice());
    }

    @Test
    public void shouldInsertInstruction_AddOnlyNewQuotes() throws TimeoutException, IOException, AppException {
        final InstructionSet first = new InstructionSet(UUID.randomUUID().toString(),
                List.of(new InstructionSet.Quote(UUID.randomUUID().toString(), BigDecimal.valueOf(12.23))));

        PostOffice.getInstance().request(
                COSMOSDB_INSERT,
                10000,
                first);

        EventEnvelope result = PostOffice.getInstance().request(
                COSMOSDB_FIND,
                10000,
                first.getInstructionSetId());

        InstructionSet instructionSet = (InstructionSet) result
                .getBody();
        assertEquals(1, instructionSet.getQuotes().size());
        assertEquals(BigDecimal.valueOf(12.23), instructionSet.getQuotes().get(0).getPrice());

        InstructionSet.Quote newQuote = new InstructionSet.Quote(UUID.randomUUID().toString(), BigDecimal.valueOf(17.33));
        InstructionSet updatedInstructionSet = instructionSet.withQuotes(
                List.of(first.getQuotes().get(0),
                        newQuote));

        PostOffice.getInstance().request(
                CosmosPojoDBPoc.COSMOSDB_ADD_QUOTE,
                10000,
                updatedInstructionSet);

        EventEnvelope updatedResult = PostOffice.getInstance().request(
                COSMOSDB_FIND,
                10000,
                first.getInstructionSetId());
        assertEquals(2, ((InstructionSet) updatedResult
                .getBody()).getQuotes().size());
        assertEquals(first.getQuotes().get(0).getId(), ((InstructionSet) updatedResult
                .getBody()).getQuotes().get(0).getId());
        assertEquals(first.getQuotes().get(0).getPrice(), ((InstructionSet) updatedResult
                .getBody()).getQuotes().get(0).getPrice());
        assertEquals(newQuote.getPrice(), ((InstructionSet) updatedResult
                .getBody()).getQuotes().get(1).getPrice());
        assertEquals(newQuote.getId(), ((InstructionSet) updatedResult
                .getBody()).getQuotes().get(1).getId());

    }

}